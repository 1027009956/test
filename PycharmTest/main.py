#导入包
#pyomo environment
from pyomo.environ import *
#pyomo optimization
from pyomo.opt import SolverFactory

#实例化一个具体model，一般的最优化问题用到的都是concrete model，而不是abstract model。
#create a model 实例化类
model = ConcreteModel()

#declare decision variables 宣布决策变量
#domain 域
model.x = Var(domain = NonNegativeReals)
model.y = Var(domain = NonNegativeReals)

#对于没有index的版本，直接列公式即可。
#expr是objective function的公式，sense是我们要最大化还是最小化
model.obj = Objective(expr=3 * model.x + 4 * model.y, sense=minimize)

#declare constraint 宣布约束条件
model.con1 = Constraint(expr=2 * model.x + model.y >= 15)

model.con2 = Constraint(expr=model.x + 2 * model.y >= 17)

model.con3 = Constraint(expr=model.x + model.y >= 10)

#SolverFactory通过使用给出的求解器名称的参数用来创建执行优化的对象。
result = SolverFactory("gurobi").solve(model)

#pprint()：Data pretty printer。模块提供了美化打印任意python数据类型的功能，让显示结果显得更加漂亮"
#print（)输出结果都在一行，不方便查看，而pprint采用了分行打印输出
model.pprint()

#solve函数将结果加载到model中，因此以下一行写出更新后的值。
model.display()
